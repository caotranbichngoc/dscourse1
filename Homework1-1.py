import datetime
now = datetime.datetime.now()
print ("Current date and time : ")
print (now.strftime("%Y-%m-%d %H:%M:%S"))
## using package datetime to retrieve current time
## in package datetime use module datetime and from this call function now()
## classmethod datetime.strptime(date_string, format)
#Return a datetime corresponding to date_string, parsed according to format. 
#This is equivalent to datetime(*(time.strptime(date_string, format)[0:6])). 
#It is not known why Jupiter notebook returns different time from Pycharm???